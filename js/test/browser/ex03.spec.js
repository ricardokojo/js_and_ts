const test = require('ava');

function scaffoldStructure(document, data) {
  const body = document.getElementsByTagName('body')[0];

  const ul = document.createElement('ul');
  body.appendChild(ul);

  data.map(val => {
    const li = document.createElement('li');
    li.classList.add('name');
    li.textContent = `<b>${val.name}</b> - <a href='${val.email}'>${val.email}</a>`;
    ul.appendChild(li)
  })
}

test('the creation of a page from scratch', t => {
  const data = [
    { name: 'Bernardo', email: 'b.ern@mail.com' },
    { name: 'Carla', email: 'carl@mail.com' },
    { name: 'Fabíola', email: 'fabi@mail.com' }
  ];

  scaffoldStructure(document, data);

  const nameNodes = document.querySelectorAll('.name');
  t.is(nameNodes.length, data.length);
});


test('document contains ul tag element', t => {
  const uls = document.getElementsByTagName('ul');

  t.true(uls.length > 0);
});

test('document contains elements with .name CSS class', t => {
  const elementsWithNameClass = document.getElementsByClassName('name');

  t.true(elementsWithNameClass.length > 0);
});

test('document contains elements in ascending alphabetical order', t => {
  const listItemElements = document.getElementsByTagName('li');

  let innerTextArray = [];
  for (let index = 0; index < listItemElements.length; index++) {
    const element = listItemElements[index];
    innerTextArray.push(element.textContent);
  };
  const innerTextArraySorted = innerTextArray.slice().sort();

  t.deepEqual(innerTextArray, innerTextArraySorted);
});

test('document contains elements with specific style', t => {
  const expectedStyle = /<(b|strong)>[a-zA-z]{0,64}<\/(b|strong)> - <a href=('|")[a-z]+@[a-z]+\.com(\.[a-z]{2})?('|")>[a-z]+@[a-z]+\.com(\.[a-z]{2})?<\/a>/;

  const listItemElements = document.getElementsByTagName('li');
  let matches = false;
  for (let index = 0; index < listItemElements.length; index++) {
    if (matches) {
      break;
    }

    const element = listItemElements[index];
    matches = expectedStyle.test(element.textContent);
  };

  t.true(matches);
});
