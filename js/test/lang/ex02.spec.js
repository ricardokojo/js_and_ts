function createUser(name, email, password, passwordConfirmation) {
  if (name.length <= 0 || name.length > 64) {
    throw new Error('validation error: invalid name size');
  }

  if (!email.match(/[a-z]+@[a-z]+\.com(\.[a-z]{2})/)) {
    throw new Error('validation error: invalid email format');
  }

  if (!password.match(/[a-z0-9]{6,20}/)) {
    throw new Error('validation error: invalid password format');
  }

  if (password !== passwordConfirmation) {
    throw new Error('validation error: confirmation does not match');
  }

  // add object return
  return {
    name: name,
    email: email,
    password: password
  };
}

describe(createUser, () => {
  test('invalid username length', () => {
    const invalidUsername = 'invalid super looong username example with more than 64 characters';

    expect(() => {
      createUser(invalidUsername);
    }).toThrowError('validation error: invalid name size');
  });
  test('invalid email format', () => {
    const invalidEmail = 'invalidemail.com';

    expect(() => {
      createUser('valid username', invalidEmail);
    }).toThrowError('validation error: invalid email format');
  });
  test('invalid password pattern', () => {
    const invalidPassword = '!pswd';

    expect(() => {
      createUser('valid username', 'email@email.com.br', invalidPassword);
    }).toThrowError('validation error: invalid password format');
  });
  test('password confirmation does not match', () => {
    const password = '123456';
    const passwordConfirmation = '654321';

    expect(() => {
      createUser('valid username', 'email@email.com.br', password, passwordConfirmation);
    }).toThrowError('validation error: confirmation does not match');
  });
  test('create a valid user', () => {
    const user = {
      name: 'valid username',
      email: 'email@email.com.br',
      password: '123456'
    };

    expect(createUser('valid username', 'email@email.com.br', '123456', '123456')).toEqual(user);
  });
});
