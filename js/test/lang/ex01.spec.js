function objectFromArrays(keys, values) {
  let obj = {};

  for (let i = 0; i < keys.length; i++) {
    obj[keys[i]] = values[i];    
  }

  return obj;
}

test('Creating objects from arrays', () => {
  const keys = ['name', 'email', 'username'];
  const values = ['Bruno', 'bruno@mail.com', 'bruno'];

  const finalObject = {
    name: 'Bruno',
    email: 'bruno@mail.com',
    username: 'bruno'
  };

  // change toBe to toEqual, as objects can't be compared by toBe function
  expect(objectFromArrays(keys, values)).toEqual(finalObject);
});

// change function name
function descending(a, b) {
  return a > b ? -1 : 1;
}

function indexer(value, index) {
  return index + '. ' + value;
}

// change function name
function shorterThan6(value) {
  return value.length < 6;
}

describe('array functions', () => {

  // changed whole test as 'reverse' didn't make sense as a sorting method
  // also, there's a Array.reverse() function to do that
  test('descending order indexed arrays', () => { // change test name
    const names = ['Marina', 'Camila', 'Alberto', 'Felipe', 'Mariana'];

    // change sort function name
    const test = names.sort(descending).map(indexer);
    // update correct order
    const correct = [
      '0. Marina',
      '1. Mariana',
      '2. Felipe',
      '3. Camila',
      '4. Alberto'
    ];
    // change toBe to toEqual in order to compare arrays
    expect(test).toEqual(correct);
  });

  test('filtering lists', () => {
    const techComps = [
      'microsoft',
      'google',
      'apple',
      'ibm',
      'amazon',
      'facebook'
    ];

    const shortNames = techComps.filter(shorterThan6);
    const correctShortNames = ['apple', 'ibm'];

    // change toBe to toEqual in order to compare arrays
    expect(shortNames).toEqual(correctShortNames);
  });
});
